package com.dmitryvi;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.*;

public class Main {


    private static String readUsingFiles(String fileName) throws IOException {

        return new String(Files.readAllBytes(Paths.get(fileName)));

    }


    private static void writeToUsingFiles(String fileName, String text) {

        try(FileWriter writer = new FileWriter(fileName, false)) {

            writer.write(text);
            writer.flush();

        }

        catch(IOException ex){

            System.out.println("File what you want to write does not exist or corrupted");

        }
    }


    private static long percentile(List<Long> values, double percentile) {

        Collections.sort(values);
        int index = (int) Math.ceil((percentile / 100) * values.size());
        return values.get(index - 1);

    }


    private static String getAverageTimeString(long avgMilliseconds) {

        Duration duration = Duration.ofMillis(avgMilliseconds);
        long hours = duration.toHours();
        duration = duration.minusHours(hours);
        long minutes = duration.toMinutes();
        return "Average is " + hours + " hours " + minutes + " minutes";

    }


    private static String getPercentileString(long percentile) {

        Duration duration = Duration.ofMillis(percentile);
        long hours = duration.toHours();
        duration = duration.minusHours(hours);
        long minutes = duration.toMinutes();
        return "Percentile is " + hours + ":" + minutes;

    }


    public static void main(String[] args) {

        try {

            Path p = Paths.get( "resources", "tickets.json");
            String path = p.toString();

            String jsonString = readUsingFiles(path);
            Object obj = new JSONParser().parse(jsonString);
            JSONObject jsonObject = (JSONObject) obj;
            JSONArray flightList = (JSONArray) jsonObject.get("tickets");
            SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yy HH:mm");
            Date departureDatetime;
            Date arrivalDatetime;
            long totalMilliseconds = 0;
            List<Long> timeOfFlyingList = new ArrayList<>();

            for (Object line : flightList) {
                String departureDatetimeString = (String) (((JSONObject) line).get("departure_date"));
                departureDatetimeString += " ";
                departureDatetimeString += (String) (((JSONObject) line).get("departure_time"));
                departureDatetime = formatter.parse(departureDatetimeString);

                String arrivalDatetimeString = (String) (((JSONObject) line).get("arrival_date"));
                arrivalDatetimeString += " ";
                arrivalDatetimeString += (String) (((JSONObject) line).get("arrival_time"));
                arrivalDatetime = formatter.parse(arrivalDatetimeString);
                long difference = arrivalDatetime.getTime() - departureDatetime.getTime();
                totalMilliseconds += (difference);
                timeOfFlyingList.add(difference);
            }

            long avgMilliseconds = totalMilliseconds / flightList.size();

            String averageTimeString = getAverageTimeString(avgMilliseconds);

            long percentile = percentile(timeOfFlyingList, 90);

            String percentileString = getPercentileString(percentile);
            String result = averageTimeString + "\n" + percentileString;

            System.out.println(result);

            Files.createDirectory(Paths.get("./results"));

            p = Paths.get( "results", "result.txt");
            path = p.toString();
            writeToUsingFiles(path, result);


        } catch (IOException e) {
            System.out.println("File does not exist or corrupted");
        } catch (ParseException e) {
            System.out.println("File format is incorrect");
        } catch (java.text.ParseException e) {
            System.out.println("Date format is incorrect");
        }

    }
}
