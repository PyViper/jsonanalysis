# JSONAnalysis

Read the inputted JSON and output average flying time and 90-percentile of flying time.

### Installation and launching

```sh
$ git clone https://gitlab.com/PyViper/jsonanalysis.git
$ cd jsonanalysis
$ mvn package
$ cd target
$ java -jar TestTask-1.0.jar
```

Answer will output into the console and into the results/result.txt